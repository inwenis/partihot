﻿using System;
using System.Collections.Generic;
using System.Windows;
using Models;

namespace Runner
{
    class ParticlesGenerator
    {
        readonly Random _random = new Random();

        public List<Particle> GenerateParticlesGrid(int numberOfRows, int numberOfColumns, double width, double height)
        {
            return GenerateParticlesGrid(numberOfRows, numberOfColumns, width, height, (a, b) => RandomSpeed(255));
        }

        public List<Particle> GenerateParticlesGridOneFastParticle(int numberOfRows, int numberOfColumns, double width, double height)
        {
            var particles = GenerateParticlesGrid(numberOfRows, numberOfColumns, width, height, (a, b) => RandomSpeed(10));
            particles[numberOfColumns].Speed = new Vector(260, 260);
            return particles;
        }

        public List<Particle> GenerateParticlesGridFastAndSlow(int numberOfRows, int numberOfColumns, double width, double height)
        {
            var fastParticlesInSecondHalfOfGrid = new Func<int, int, Vector>((row, column) =>
            {
                Vector speed;
                if (column < numberOfColumns / 2)
                    speed = RandomSpeed(10);
                else
                    speed = RandomSpeed(200) * 4;
                return speed;
            });
            var particles = GenerateParticlesGrid(numberOfRows, numberOfColumns, width, height, fastParticlesInSecondHalfOfGrid);
            return particles;
        }

        private Vector RandomSpeed(int span)
        {
            return new Vector
            {
                X = (_random.NextDouble() * span) - (span / 2),
                Y = (_random.NextDouble() * span) - (span / 2),
            };
        }

        private List<Particle> GenerateParticlesGrid(int numberOfRows, int numberOfColumns, double width, double height, Func<int, int, Vector> getSpeed)
        {
            var particles = new List<Particle>();
            int counter = 0;
            double werticalSpanBetwenParticles = width / (numberOfColumns + 1);
            double horizontalSpanBetwenParticles = height / (numberOfRows + 1);
            for (int row = 0; row < numberOfRows; ++row)
            {
                for (int column = 0; column < numberOfColumns; ++column)
                {
                    var particle = new Particle
                    {
                        Id = counter,
                        Mass = 1,
                        Radius = 10,
                        Speed = getSpeed(row, column),
                        Position =
                        {
                            X = werticalSpanBetwenParticles * (column + 1),
                            Y = horizontalSpanBetwenParticles * (row + 1)
                        }
                    };
                    particles.Add(particle);
                    ++counter;
                }
            }
            return particles;
        }
    }
}