﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Models;

namespace Runner.Commands
{
    internal class DeserializeCommand : ICommand
    {
        private readonly InMemoryDataBase _dataBase;

        public DeserializeCommand(InMemoryDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get { return new[] { "deserialize" }; }
        }

        public string Name
        {
            get { return "Deserialize"; }
        }

        public void Execute(string input)
        {
            Console.WriteLine("Reading object from file");
            var stream = File.Open("simulation.osl", FileMode.Open);
            var bformatter = new BinaryFormatter();
            var simulation = (Simulation)bformatter.Deserialize(stream);
            stream.Close();
            Console.WriteLine("Done!");
            var id = _dataBase.AddSimulation(simulation);
            Console.WriteLine("Added simulation with id {0}", id.ToString("N"));
        }
    }
}