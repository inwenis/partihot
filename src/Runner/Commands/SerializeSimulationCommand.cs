﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Models;
using Runner.Commands.Helpers;

namespace Runner.Commands
{
    internal class SerializeSimulationCommand : ICommand
    {
        private readonly InMemoryDataBase _dataBase;

        public SerializeSimulationCommand(InMemoryDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "serialize"
                };
            }
        }

        public string Name { get { return "SerializeSimulation"; } }

        public Option[] Options
        {
            get
            {
                return new[]
                {
                    new Option{Name = "id", IsMandatory = true}, 
                };
            }
        }

        public void Execute(string input)
        {
            
            var parsedInput = InputParser.ParseInput(input);
            InputValidator.ValidateInput(parsedInput, Options);

            var id = Guid.Parse(parsedInput["id"]);

            var simulation = _dataBase.GetSimulation(id);
            Serialize(simulation);
        }

        private void Serialize(Simulation simulation)
        {
            Console.WriteLine("Will now serialize");
            var stream = File.Open("simulation.osl", FileMode.Create);
            var bformatter = new BinaryFormatter();
            bformatter.Serialize(stream, simulation);
            stream.Close();
            Console.WriteLine("Done!");
        }
    }
}