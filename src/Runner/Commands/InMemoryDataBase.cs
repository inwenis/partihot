﻿using System;
using System.Collections.Generic;
using Models;

namespace Runner.Commands
{
    internal class InMemoryDataBase
    {
        private readonly Dictionary<Guid, Simulation> _simulations;

        public InMemoryDataBase()
        {
            _simulations = new Dictionary<Guid, Simulation>();
        }

        public Guid AddSimulation(Simulation simulation)
        {
            var id = Guid.NewGuid();
            _simulations.Add(id, simulation);
            return id;
        }

        public Simulation GetSimulation(Guid id)
        {
            var simulation = _simulations[id];
            return simulation;
        }
    }
}