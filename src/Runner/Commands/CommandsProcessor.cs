﻿using System.Collections.Generic;
using System.Linq;

namespace Runner.Commands
{
    class CommandsProcessor
    {
        private List<ICommand> _commands;
        private readonly InMemoryDataBase _dataBase = new InMemoryDataBase();

        public ICommand CommandNotFoundCommand { get; set; }
        public ICommand MoreThanOneCommandMatchesAlias { get; set; }

        public void Initialize()
        {
            var createSimulationCommand = new CreateSimulationCommand(_dataBase);
            var deserializeCommand = new DeserializeCommand(_dataBase);
            _commands = new List<ICommand>
            {
                new ClearCommand(),
                new ExitCommand(),
                createSimulationCommand,
                new CreateSampleSimulationCommand(createSimulationCommand),
                new SerializeSimulationCommand(_dataBase),
                deserializeCommand,
                new RunSimulation(_dataBase, deserializeCommand),
                new TestCommand()
            };

            _commands.Add(new HelpCommand(_commands));

            CommandNotFoundCommand = new CommandNotFoundCommand(_commands);
            MoreThanOneCommandMatchesAlias = new VerboseCommand("More than one command matches the given allias");
        }

        public ICommand RecognizeCommand(string input)
        {
            var commandAlias = input.Split(' ').First();
            var recognizedCommands = new List<ICommand>();
            foreach (var command in _commands)
            {
                if (command.Alliases.Contains(commandAlias))
                    recognizedCommands.Add(command);
            }

            var recognizedCommandsCount = recognizedCommands.Count;

            switch (recognizedCommandsCount)
            {
                case 0:
                    return CommandNotFoundCommand;
                case 1:
                    return recognizedCommands.Single();
                default:
                    return MoreThanOneCommandMatchesAlias;
            }
        }
    }
}