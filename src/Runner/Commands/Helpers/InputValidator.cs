﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Runner.Commands.Helpers
{
    internal class InputValidator
    {
        public static void ValidateInput(IReadOnlyDictionary<string, string> parsedInput, Option[] options)
        {
            foreach (var option in options.Where(o => o.IsMandatory))
            {
                if (false == parsedInput.ContainsKey(option.Name))
                {
                    throw new Exception(String.Format("Mandatory option {0} not supplied", option.Name));
                }
            }
        }
    }
}