﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Runner.Commands.Helpers
{
    class InputParser
    {
        private static readonly char[] OptionIndicationCharacters = { '-', '/' };

        public static IReadOnlyDictionary<string, string> ParseInput(string input)
        {
            var commandInvocationStringAndOptions = Regex.Match(input, @"(\w+)\W(.*)");
            var commandInvocationString = commandInvocationStringAndOptions.Groups[1].Value;
            var options = commandInvocationStringAndOptions.Groups[2].Value;

            var splitedOptions = options.Split(OptionIndicationCharacters).Where(t => false == String.IsNullOrEmpty(t));

            var result = new Dictionary<string, string>();

            foreach (var option in splitedOptions)
            {
                var optionNameAndValue = Regex.Match(option, @"(\w+)\W(.*)");
                if (optionNameAndValue.Success)
                {
                    var optionName = optionNameAndValue.Groups[1].Value;
                    var optionValue = optionNameAndValue.Groups[2].Value;
                    result.Add(optionName, optionValue);
                }
                else
                {
                    throw new Exception(string.Format("Could not extract name and value from {0}. Remember not to use - in values", option));
                }
            }
            return result;
        }
    }
}