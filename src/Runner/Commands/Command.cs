﻿using System.Collections.Generic;

namespace Runner.Commands
{
    class ExitCommand : ICommand
    {
        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "exit",
                    "quit"
                };
            }
        }

        public virtual string Name
        {
            get { return "Exit"; }
        }

        public virtual void Execute(string input)
        {
            Program.Exit = true;
        }
    }
}