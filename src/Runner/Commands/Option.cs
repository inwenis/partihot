﻿namespace Runner.Commands
{
    //add options and flags
    internal class Option
    {
        public string Name;
        public bool IsMandatory;
    }
}