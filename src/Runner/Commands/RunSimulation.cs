﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Models;
using PartiHot;
using Runner.Commands.Helpers;

namespace Runner.Commands
{
    internal class RunSimulation : ICommand
    {
        private readonly InMemoryDataBase _dataBase;
        private readonly DeserializeCommand _deserializeCommand;

        public RunSimulation(InMemoryDataBase dataBase, DeserializeCommand deserializeCommand)
        {
            _dataBase = dataBase;
            _deserializeCommand = deserializeCommand;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get { return new[] { "run" }; }
        }

        public string Name
        {
            get { return "Run"; }
        }

        public void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            if (parsedInput.ContainsKey("file"))
            {
                Console.WriteLine("Reading object from file");
                var stream = File.Open("simulation.osl", FileMode.Open);
                var bformatter = new BinaryFormatter();
                var simulation = (Simulation)bformatter.Deserialize(stream);
                stream.Close();
                Console.WriteLine("Done!");

                var visualizer = new Visualizer(simulation);
                var collider = new Collider(simulation);
                var simulator = new Simulator(simulation, visualizer, collider);
                simulator.DoSimulation();
            }
        }
    }
}