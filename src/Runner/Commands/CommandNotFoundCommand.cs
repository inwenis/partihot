﻿using System;
using System.Collections.Generic;

namespace Runner.Commands
{
    internal class CommandNotFoundCommand : ICommand
    {
        private readonly IEnumerable<ICommand> _availableCommands;

        public CommandNotFoundCommand(IEnumerable<ICommand> availableCommands)
        {
            _availableCommands = availableCommands;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get { throw new NotImplementedException(); }
        }

        public virtual string Name
        {
            get { throw new NotImplementedException(); }
        }

        public virtual void Execute(string input)
        {
            Console.WriteLine("The command has not been found, have you added it in CommandsProcessor?");
            Console.WriteLine("Available commands are:");
            foreach (var availableCommand in _availableCommands)
            {
                Console.WriteLine(availableCommand.Name);
            }
        }
    }
}