﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Runner.Commands
{
    class HelpCommand : ICommand
    {
        private readonly IEnumerable<ICommand> _availableCommands;

        public HelpCommand(IEnumerable<ICommand> availableCommands)
        {
            _availableCommands = availableCommands;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "help",
                    "?"
                };
            }
        }

        public virtual string Name
        {
            get { return "Help"; }
        }

        public virtual void Execute(string input)
        {
            Console.WriteLine("Available commands are:");
            foreach (var availableCommand in _availableCommands)
            {
                Console.WriteLine("\t-{0} - {1}", availableCommand.Alliases.First(), availableCommand.Name);
            }
        }
    }
}