﻿using System;
using System.Collections.Generic;

namespace Runner.Commands
{
    internal class ClearCommand : ICommand
    {
        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "clear",
                    "cls"
                };
            }
        }

        public string Name
        {
            get { return "ClearCommand"; }
        }

        public void Execute(string input)
        {
            Console.Clear();
        }
    }
}