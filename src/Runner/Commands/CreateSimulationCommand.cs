﻿using System;
using System.Collections.Generic;
using Models;
using Runner.Commands.Helpers;

namespace Runner.Commands
{
    internal class CreateSimulationCommand : ICommand
    {
        private readonly InMemoryDataBase _dataBase;

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "createsimulation",
                    "newsimulation"
                };
            }
        }

        public string Name
        {
            get { return "CreateSimulation"; }
        }

        public Option[] Options
        {
            get
            {
                return new[]
                {
                    new Option{Name = "numberOfFrames", IsMandatory = true}, 
                    new Option{Name = "timeIntervall", IsMandatory = true}, 
                    new Option{Name = "width", IsMandatory = true}, 
                    new Option{Name = "height", IsMandatory = true}, 
                    new Option{Name = "layout", IsMandatory = true}, 
                };
            }
        }

        public CreateSimulationCommand(InMemoryDataBase dataBase)
        {
            _dataBase = dataBase;
        }

        public void Execute(string input)
        {
            var parsedInput = InputParser.ParseInput(input);
            InputValidator.ValidateInput(parsedInput, Options);
            
            var particlesGenerator = new ParticlesGenerator();

            Particle[] particles = null;
//            switch (parsedInput["layout"])
//            {
//                case "grid":
                    particles = particlesGenerator.GenerateParticlesGridFastAndSlow(5, 5, 700, 500).ToArray();
//                    break;
//            }

            var simulation = new Simulation
            {
                NumberOfFrames = Int32.Parse(parsedInput["numberOfFrames"]),
                TimeUnit = Double.Parse(parsedInput["timeIntervall"]),
                ContainerWidth = Int32.Parse(parsedInput["width"]),
                ContainerHeight = Int32.Parse(parsedInput["height"]),
                Particles = particles
            };

            var newSimulationsId = _dataBase.AddSimulation(simulation);
            Console.WriteLine("Simulation with id {0} added", newSimulationsId.ToString("N"));
        }
    }
}