﻿using System;
using System.Collections.Generic;

namespace Runner.Commands
{
    internal class VerboseCommand : ICommand
    {
        private readonly string _message;

        public VerboseCommand(string message)
        {
            _message = message;
        }

        public virtual IReadOnlyCollection<string> Alliases
        {
            get { throw new NotImplementedException(); }
        }

        public virtual string Name
        {
            get { throw new NotImplementedException(); }
        }

        public virtual void Execute(string input)
        {
            Console.WriteLine(_message);
        }
    }
}