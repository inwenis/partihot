﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Models;
using PartiHot;

namespace Runner.Commands
{
    internal class TestCommand : ICommand
    {
        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "test"
                };
            }
        }

        public string Name
        {
            get { return "Test"; }
        }

        public void Execute(string input)
        {
            var particlesGenerator = new ParticlesGenerator();

            var particles = particlesGenerator.GenerateParticlesGridFastAndSlow(5, 5, 300, 200).ToArray();

            var simulation = new Simulation
            {
                NumberOfFrames = 200,
                TimeUnit = 0.1,
                ContainerWidth = 300,
                ContainerHeight = 200,
                Particles = particles
            };

            var visualizer = new Visualizer(simulation);
            var collider = new Collider(simulation);

            var stopwatch = new Stopwatch();

            var simulator = new Simulator(simulation, visualizer, collider);
            stopwatch.Start();
            simulator.DoSimulation();

            stopwatch.Stop();
            Console.WriteLine("Elapsed={0}", stopwatch.Elapsed);
        }
    }
}