﻿using System.Collections.Generic;

namespace Runner.Commands
{
    internal class CreateSampleSimulationCommand : ICommand
    {
        private readonly CreateSimulationCommand _createSimmulationCommand;

        public CreateSampleSimulationCommand(CreateSimulationCommand createSimmulationCommand)
        {
            _createSimmulationCommand = createSimmulationCommand;
        }

        public IReadOnlyCollection<string> Alliases
        {
            get
            {
                return new[]
                {
                    "sample",
                    "samplesimulation"
                };
            }
        }

        public string Name
        {
            get { return "SampleSimulation"; }
        }

        public void Execute(string input)
        {
            _createSimmulationCommand.Execute("createsimulation -width 200 -height 300 -layout grid -numberOfFrames 100 -timeIntervall 0,1");
        }
    }
}