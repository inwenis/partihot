﻿using System;
using Models;
using PartiHot;
using Runner.Commands;

namespace Runner
{
    class Program
    {
        public static bool Exit;
        private static CommandsProcessor _commandsProcessor;

        static void Main()
        {
            Initialize();

            while (Exit == false)
            {
                var input = Console.ReadLine();
                var command = _commandsProcessor.RecognizeCommand(input);
//                try
//                {
//                    command.Execute(input);
//                }
//                catch (Exception e)
//                {
//                    HandleException(e);
//                }
                command.Execute(input);

            }
        }

        private static void Initialize()
        {
            _commandsProcessor = new CommandsProcessor();
            _commandsProcessor.Initialize();
        }

        private static void HandleException(Exception exception)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            var message = string.Format("The command has thrown an exception: {0}", exception.Message);
            Console.WriteLine(message);
            Console.WriteLine();
            Console.WriteLine(exception.StackTrace);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
