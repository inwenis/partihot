﻿using System;
using System.Windows;
using Models;

namespace PartiHot
{
    public class Simulator
    {
        private readonly Collider _collider;
        private readonly int _numberOfFrames;
        private readonly double _timeUnit;
        private readonly int _containerWidth;
        private readonly int _containerHeight;
        private readonly Visualizer _visualizer;
        private readonly Particle[] _particles;

        public Simulator(Simulation simulation, Visualizer visualizer, Collider collider)
        {
            _visualizer = visualizer;
            _collider = collider;
            _timeUnit = simulation.TimeUnit;
            _numberOfFrames = simulation.NumberOfFrames;
            _containerWidth = simulation.ContainerWidth;
            _containerHeight = simulation.ContainerHeight;
            _particles = simulation.Particles;
        }

        public void DoSimulation()
        {
            double time = 0;

            var collisions = _collider.InitializeCollisions(_particles);

            var nextCollision = FindNextCollision(collisions, new Vector(-1, -1));

            for (int i = 0; i < _numberOfFrames; ++i)
            {
                _visualizer.AddFrame(_particles);
                if (time + _timeUnit < nextCollision.Time)
                {
                    Move(_particles, _timeUnit);
                }
                else
                {
                    nextCollision = MoveAndCollide(collisions, _particles, _timeUnit, time, nextCollision);
                }
                Console.WriteLine("{0}/{1}", i, _numberOfFrames);
                time += _timeUnit;
            }

            _visualizer.SaveGIF("out.gif");
        }

        private static Collision FindNextCollision(Collision[,] collisions, Vector lastCollision)
        {
            var currentClosestCollision = collisions[0, 0];
            var numberOfParticles = collisions.GetLength(0);
            for (int i = 0; i < numberOfParticles; ++i)
            {
                for (int j = 0; j <= i; ++j)
                {
                    if (collisions[i, j].Time < currentClosestCollision.Time && !WasPreviousCollision(collisions[i, j], lastCollision))
                    {
                        currentClosestCollision = collisions[i, j];
                    }
                }
            }
            return currentClosestCollision;
        }

        private static bool WasPreviousCollision(Collision collision, Vector lastCollision)
        {
            if (collision is ParticleParticleCollsion)
                return WasPreviousCollision(collision as ParticleParticleCollsion, lastCollision);
            else
                return WasPreviousCollision(collision as ParticleWallCollsion, lastCollision);

        }

        private static bool WasPreviousCollision(ParticleParticleCollsion collision, Vector lastCollision)
        {
            if ((collision.Particle.Id == lastCollision.X && collision.SecondParticle.Id == lastCollision.Y) ||
                (collision.Particle.Id == lastCollision.Y && collision.SecondParticle.Id == lastCollision.X))
            {
                return true;
            }
            return false;
        }

        private static bool WasPreviousCollision(ParticleWallCollsion collision, Vector lastCollision)
        {
            return false;
        }

        private static void Move(Particle[] particlesArray, double timeUnit)
        {
            for (int i = 0; i < particlesArray.Length; ++i)
            {
                var particle = particlesArray[i];
                particle.Position.X += particle.Speed.X * timeUnit;
                particle.Position.Y += particle.Speed.Y * timeUnit;
                particlesArray[i] = particle;
            }
        }

        private Collision MoveAndCollide(Collision[,] collisions, Particle[] particles, double timeUnit, double time, Collision collision)
        {
            var lastCollision = new Vector(-1, -1);
            lastCollision.X = collision.Particle.Id;
            if (collision is ParticleParticleCollsion)
                lastCollision.Y = (collision as ParticleParticleCollsion).SecondParticle.Id;
            else
                lastCollision.Y = -1;
            double startTime = time;
            do
            {
                double timeToCollision = collision.Time - time;
                if (timeToCollision != 0)
                    Move(particles, timeToCollision);
                time = collision.Time;
                if (collision is ParticleParticleCollsion)
                {
                    Collision(collision as ParticleParticleCollsion);
                    UpdateParticlesCollisionWithParticles(particles, collisions, (collision as ParticleParticleCollsion).SecondParticle, time);
                    UpdateParticlesCollisionWithWall(collisions, (collision as ParticleParticleCollsion).SecondParticle, time);
                }
                else
                {
                    Collision(collision as ParticleWallCollsion);
                }
                UpdateParticlesCollisionWithParticles(particles, collisions, collision.Particle, time);
                UpdateParticlesCollisionWithWall(collisions, collision.Particle, time);


                collision = FindNextCollision(collisions, lastCollision);
                lastCollision.X = collision.Particle.Id;
                if (collision is ParticleParticleCollsion)
                    lastCollision.Y = (collision as ParticleParticleCollsion).SecondParticle.Id;
                else
                    lastCollision.Y = -1;
            }
            while (startTime + timeUnit > collision.Time);
            if (time != startTime + timeUnit)
                Move(particles, startTime + timeUnit - time);
            return collision;
        }

        private void UpdateParticlesCollisionWithParticles(Particle[] particles, Collision[,] collisions, Particle particle, double time)
        {
            int numberOfParticles = particles.Length;
            for (int i = 0; i < numberOfParticles; ++i)
            {
                if (i != particle.Id)
                {
                    _collider.SetCollisionTime(collisions[i, particle.Id] as ParticleParticleCollsion);
                    collisions[i, particle.Id].Time += time;
                }
            }
        }

        private void UpdateParticlesCollisionWithWall(Collision[,] collisionsArray, Particle particle, double time)
        {
            _collider.SetCollisionTimeAndWall(collisionsArray[particle.Id, particle.Id] as ParticleWallCollsion);
            collisionsArray[particle.Id, particle.Id].Time += time;
        }

        private void Collision(ParticleParticleCollsion collsion)
        {
            var particle1 = collsion.Particle;
            var particle2 = collsion.SecondParticle;

            Vector momentum1 = particle1.Mass * particle1.Speed;
            Vector momentum2 = particle2.Mass * particle2.Speed;

            Vector momentum21 = momentum2 - momentum1; //momentum of 2nd particle in physical system of 1st particle

            Vector radiusesvector = particle1.Position - particle2.Position;

            //angle between momentum21 and line segment created by radiuses of particles
            double cosalfa = (radiusesvector.X * momentum21.X + radiusesvector.Y * momentum21.Y) / (radiusesvector.Length * momentum21.Length);

            double passedMomentumLength = cosalfa * momentum21.Length;
            Vector passedMomentum = radiusesvector * passedMomentumLength / radiusesvector.Length;

            particle1.Speed += passedMomentum / particle1.Mass;
            particle2.Speed -= passedMomentum / particle2.Mass;
        }

        private void Collision(ParticleWallCollsion collsion)
        {
            switch (collsion.Wall)
            {
                case Wall.Bottom:
                case Wall.Top:
                    collsion.Particle.Speed.Y *= -1;
                    break;
                case Wall.Right:
                case Wall.Left:
                    collsion.Particle.Speed.X *= -1;
                    break;
            }
        }
    }
}
