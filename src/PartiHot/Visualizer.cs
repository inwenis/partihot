﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using Models;

namespace PartiHot
{
    public class Visualizer
    {
        private readonly List<Bitmap> _images = new List<Bitmap>();
        private readonly int _width;
        private readonly int _height;

        public Visualizer(Simulation simulation)
        {
            _width = simulation.ContainerWidth;
            _height = simulation.ContainerHeight;
        }

        public void AddFrame(Particle[] particles)
        {
            var bitmap = new Bitmap(_width, _height);

            var whiteBrush = new SolidBrush(Color.White);
            var blackPen = new Pen(Color.Black);

            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.FillRectangle(whiteBrush, 0, 0, _width, _height);
                DrawFrame(graphics, blackPen);
                foreach (var particle in particles)
                {
                    var x = (int)(particle.Position.X - particle.Radius);
                    var y = (int)(particle.Position.Y - particle.Radius);
                    var diameter = (int)particle.Radius * 2;
                    graphics.FillEllipse(new SolidBrush(SpeedToGrayScaleColor(particle.Speed)), x, y, diameter, diameter);
                }
            }
            _images.Add(bitmap);
        }

        private void DrawFrame(Graphics graphics, Pen blackPen)
        {
            graphics.DrawRectangle(blackPen, 0, 0, _width - 1, _height - 1);
        }

        private static Color SpeedToGrayScaleColor(Vector speed)
        {
            var r = (int)Math.Min(speed.Length, 235);
            return Color.FromArgb(r, r, r);
        }

        public void SaveGIF(string fileName)
        {
            var gifBitmapEncoder = new GifBitmapEncoder();

            Console.WriteLine("Creating GIF...");
            var framesCount = _images.Count;
            var counter = 0;
            foreach (var bmpImage in _images)
            {
                var frame = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap
                    (
                        bmpImage.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions()
                    );
                gifBitmapEncoder.Frames.Add(BitmapFrame.Create(frame));
                Console.WriteLine("{0}/{1}", counter++, framesCount);
            }
            gifBitmapEncoder.Save(new FileStream(fileName, FileMode.Create));
        }

    }
}