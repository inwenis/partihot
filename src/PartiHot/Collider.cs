﻿using System;
using System.Linq;
using Models;

namespace PartiHot
{
    public class Collider
    {
        private Collision[,] _collisions;
        private readonly int _width;
        private readonly int _height;

        public Collider(Simulation simulation)
        {
            _width = simulation.ContainerWidth;
            _height = simulation.ContainerHeight;
        }

        public Collision[,] InitializeCollisions(Particle[] particles)
        {
            var particlesCount = particles.Count();
            _collisions = new Collision[particlesCount, particlesCount];

            InitializeParticleParticleCollisions(particles);
            InitializePartilceWallColizions(particles);

            return _collisions;
        }

        private void InitializeParticleParticleCollisions(Particle[] particles)
        {
            var particlesCount = particles.Count();
            for (int i = 0; i < particlesCount; ++i)
            {
                for (int j = 0; j < i; ++j)
                {
                    var collision = new ParticleParticleCollsion
                    {
                        Particle = particles[i],
                        SecondParticle = particles[j]
                    };
                    SetCollisionTime(collision);
                    _collisions[i, j] = collision;
                    _collisions[j, i] = collision;
                }
            }
        }

        private void InitializePartilceWallColizions(Particle[] particles)
        {
            int particlesCount = particles.Count();
            for (int i = 0; i < particlesCount; ++i)
            {
                var collision = new ParticleWallCollsion
                {
                    Particle = particles[i]
                };
                SetCollisionTimeAndWall(collision);
                _collisions[i, i] = collision;
            }
        }

        public void SetCollisionTimeAndWall(ParticleWallCollsion collision)
        {
            var particle = collision.Particle;

            int xCoordinateOfRightVerticalWall = _width;
            int xCoordinateOfLeftVerticalWall = 0;
            int yCoordinateOfBttomHorizontalWall = _height;
            int yCoordinateOftopHorizontalWall = 0;

            double timeToCollisionWithVerticalWall = double.MaxValue;
            double timeToCollisionWithHorizontalWall = double.MaxValue;
            double time = double.MaxValue;
            Wall typeX, typeY;
            typeX = typeY = Wall.Top;
            if (particle.Speed.X > 0)
            {
                timeToCollisionWithVerticalWall = (xCoordinateOfRightVerticalWall - particle.Position.X - particle.Radius) / particle.Speed.X;
                typeX = Wall.Right;
            }
            else
            {
                if (particle.Speed.X < 0)
                {
                    timeToCollisionWithVerticalWall = (xCoordinateOfLeftVerticalWall - particle.Position.X + particle.Radius) / particle.Speed.X;
                    typeX = Wall.Left;
                }
            }
            if (particle.Speed.Y > 0)
            {
                timeToCollisionWithHorizontalWall = (yCoordinateOfBttomHorizontalWall - particle.Position.Y - particle.Radius) / particle.Speed.Y;
                typeY = Wall.Bottom;
            }
            else
            {
                if (particle.Speed.Y < 0)
                {
                    timeToCollisionWithHorizontalWall = (yCoordinateOftopHorizontalWall - particle.Position.Y + particle.Radius) / particle.Speed.Y;
                    typeY = Wall.Top;
                }
            }
            if (timeToCollisionWithVerticalWall < 0)
            {
                collision.Wall = typeY;
                collision.Time = timeToCollisionWithHorizontalWall;
            }
            if (timeToCollisionWithHorizontalWall < 0)
            {
                collision.Wall = typeX;
                collision.Time = timeToCollisionWithVerticalWall;
            }
            if (timeToCollisionWithVerticalWall < timeToCollisionWithHorizontalWall)
            {
                collision.Wall = typeX;
                time = timeToCollisionWithVerticalWall;
            }
            else
            {
                collision.Wall = typeY;
                time = timeToCollisionWithHorizontalWall;
            }
            collision.Time = time;
        }

        public void SetCollisionTime(ParticleParticleCollsion collsion)
        {
            var p1 = collsion.Particle;
            var p2 = collsion.SecondParticle;
            //t^2 * (dv_x^2+dv_y^2)+ t*2*(dv_x*dx+dv_y*dy)+dx^2+dy^2= (2*r)^2
            double dv_x = p2.Speed.X - p1.Speed.X;
            double dv_y = p2.Speed.Y - p1.Speed.Y;
            double dx = p2.Position.X - p1.Position.X;
            double dy = p2.Position.Y - p1.Position.Y;
            double b = 2 * dv_x * dx + dv_y * dy;
            double a = dv_x * dv_x + dv_y * dv_y;
            double c = dx * dx + dy * dy - Math.Pow(p1.Radius + p2.Radius, 2);
            double delta = Math.Pow(b, 2) - 4 * a * c;
            collsion.Time = double.MaxValue;
            if (delta >= 0)
            {
                double x1 = (-b - Math.Sqrt(delta)) / a;
                double x2 = (-b + Math.Sqrt(delta)) / a;
                if (x1 > x2)
                {
                    double tmp = x1;
                    x1 = x2;
                    x2 = tmp;
                }
                if (x2 > 0)
                {
                    collsion.Time = x2;
                }
                if (x1 > 0)
                {
                    collsion.Time = x1;
                }
            }
        }
    }
}