﻿using System;

namespace Models
{
    [Serializable]
    public class Simulation
    {
        public Particle[] Particles;
        public int NumberOfFrames;
        public double TimeUnit;
        public int ContainerWidth;
        public int ContainerHeight;
    }
}
