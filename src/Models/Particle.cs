﻿using System;
using System.Windows;

namespace Models
{
    [Serializable]
    public class Particle
    {
        public int Id;
        public double Radius;
        public Vector Position;
        public Vector Speed;
        public float Mass;
    }
}