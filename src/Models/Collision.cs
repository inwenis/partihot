﻿namespace Models
{
    public abstract class Collision
    {
        public double Time;
        public Particle Particle;
    }
}
