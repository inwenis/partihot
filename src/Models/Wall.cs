﻿namespace Models
{
    public enum Wall
    {
        Right,
        Top,
        Left,
        Bottom
    }
}